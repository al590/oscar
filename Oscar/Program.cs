﻿using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using Oscar.Providers;

namespace Oscar
{
    internal class Program
    {
        private ConfigProvider _config;
        private DiscordSocketClient _client;
        private CommandService _commands;
        private IServiceProvider _services;

        internal async Task Start()
        {
            if (!LoadConfig("Oscar.cfg"))
                return;

            _client = new DiscordSocketClient();
            _client.Log += OnLog;
            _client.MessageReceived += OnMessageReceived;

            _commands = new CommandService(new CommandServiceConfig
            {
                CaseSensitiveCommands = false
            });
            _commands.Log += OnLog;
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly());

            _services = new ServiceCollection()
                .AddSingleton(_client)
                .AddSingleton(_commands)
                .AddSingleton(_config)
                .AddSingleton(new DatabaseProvider(_config))
                .AddSingleton(new AudioProvider(_config))
                .BuildServiceProvider();

            await _client.LoginAsync(TokenType.Bot, _config.Token);
            await _client.StartAsync();
            await _client.SetGameAsync($"{_config.CommandPrefix} help");

            // Need to find a little bit elegant of a way to do this...
            while (_client.ConnectionState != ConnectionState.Connected)
                await Task.Delay(1000);

            while (_client.ConnectionState != ConnectionState.Disconnected)
                await Task.Delay(1000);
        }

        internal bool LoadConfig(string configName)
        {
            _config = new ConfigProvider(configName);

            try
            {
                if (File.Exists(configName))
                {
                    _config.Load();

                    if (_config.IsValid())
                        return true;
                    else
                        Console.WriteLine("Config failed validation; aborting...");
                }
                else
                {
                    _config.Save();
                    Console.WriteLine(
                        "Missing config file!\n" +
                        "An empty configuration file has been generated for you.\n" +
                        "Please configure the bot as needed then re-launch Oscar."
                    );
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error loading config:\n{e}");
            }

            return false;
        }

        #region Event Handlers
        
        private async Task OnMessageReceived(SocketMessage message)
        {
            var argPos = 0;

            if (!(message is SocketUserMessage userMessage) || 
                userMessage.Source != MessageSource.User || 
                !userMessage.HasStringPrefix($"{_config.CommandPrefix} ", ref argPos))
                return;

            var command = userMessage.Content.Substring(argPos).ToLower();
            var additionalArgs = command.IndexOf(' ');

            if (additionalArgs != -1)
                command = command.Substring(0, additionalArgs);

            if (_config.DisabledCommands.Contains(command))
            {
                await userMessage.Channel.SendMessageAsync($"{command} is disabled.").ConfigureAwait(false);
                return;
            }

            var context = new SocketCommandContext(_client, userMessage);
            var result = await _commands.ExecuteAsync(context, argPos, _services);

            if (result.Error.HasValue && result.Error.Value != CommandError.UnknownCommand)
                await context.Channel.SendMessageAsync(result.ToString()).ConfigureAwait(false);
        }

        private Task OnLog(LogMessage message)
        {
            Console.WriteLine($"[{message.Severity}] {message.Message} ({message.Source})");
            return Task.CompletedTask;
        }

        #endregion

        internal static void Main() => new Program().Start().GetAwaiter().GetResult();
    }
}
