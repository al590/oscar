﻿using System;

namespace Oscar
{
    public static class ExtensionMethods
    {
        public static string After(this string haystack, char needle)
        {
            var index = haystack.IndexOf(needle);

            if (index == -1)
                return null;

            var adjustedIndex = index + 1;
                
            return adjustedIndex == haystack.Length
                ? "" 
                : haystack.Substring(adjustedIndex);
        }
    }
}
