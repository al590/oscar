﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Oscar.Providers;

namespace Oscar.Modules
{
    public class GeneralModule : ModuleBase
    {
        private static readonly TimeSpan NormalCooldown = TimeSpan.FromSeconds(5);

        private static readonly Dictionary<ulong, DateTime> _cooldowns = new Dictionary<ulong, DateTime>();
        private readonly CommandService _commands;
        private readonly ConfigProvider _config;
        private readonly DatabaseProvider _db;

        public GeneralModule(CommandService commands, DatabaseProvider db, ConfigProvider config)
        {
            _commands = commands;
            _db = db;
            _config = config;
        }

        #region Commands

        [Command("corgi")]
        [Summary("Posts a picture of an adorable corgi!")]
        public async Task Corgi() => await ReplyWithRandomImage("corgi", NormalCooldown).ConfigureAwait(false);

        [Command("enable")]
        [Summary("Enables a command. Only the bot's owner may use this.")]
        public async Task EnableCommand(string command) => await SetCommandEnabled(command, true).ConfigureAwait(false);

        [Command("disable")]
        [Summary("Disables a command. Only the bot's owner may use this.")]
        public async Task DisableCommand(string command) => await SetCommandEnabled(command, false).ConfigureAwait(false);

        [Command("help")]
        public async Task Help(string target = null)
        {
            if (string.IsNullOrEmpty(target))
            {
                var response = new StringBuilder();
                var commandsSorted = _commands.Commands.OrderBy(cmd => cmd.Name, StringComparer.OrdinalIgnoreCase).Select(x => x.Name);
                response.Append(string.Join(", ", commandsSorted));
                response.Append($"\n\nFor more information on a particular command, use {_config.CommandPrefix} help command_name");

                var embed = new EmbedBuilder()
                    .WithDescription(response.ToString())
                    .WithTitle("Commands")
                    .Build();
                await ReplyAsync("", false, embed).ConfigureAwait(false);
            }
            else
            {
                var command = _commands.Commands.FirstOrDefault(x => x.Name.Equals(target, StringComparison.OrdinalIgnoreCase));

                if (command != null)
                {
                    var aliases = command.Aliases?.Count > 0 
                        ? $" ({string.Join(", ", command.Aliases)})"
                        : "";
                    await ReplyAsync($"{command.Name}{aliases} - {command.Summary}").ConfigureAwait(false);
                }
                else
                    await ReplyAsync("i don't have a command with that name dude :eyes:").ConfigureAwait(false);
            }
        }

        [Command("meme")]
        [Summary("Posts a random meme.")]
        public async Task Meme() => await ReplyWithRandomImage("meme", NormalCooldown).ConfigureAwait(false);

        [Command("quit")]
        [Summary("Shuts Oscar down. Only the owner can use this command.")]
        public async Task Quit()
        {
            if (Context.User.Id == _config.OwnerID)
            {
                await ReplyAsync("Bye!").ConfigureAwait(false);
                _config.Save();
                #pragma warning disable 4014
                Context.Client.StopAsync();
                #pragma warning restore 4014
            }
            else
                await ReplyAsync("Nice try.").ConfigureAwait(false);
        }

        #endregion

        #region Helpers

        private async Task ReplyWithRandomImage(string category, TimeSpan? cooldown = null)
        {
            var image = await _db.FindRandomImageAsync(category);

            if (image == null)
            {
                await ReplyAsync("Wew lad, something went wrong finding an image :( Sorry about that!");
                return;
            }

            if (cooldown.HasValue)
            {
                // Could see about adding whitelisted users instead of just the owner
                var ignoreCooldown = _config.IgnoreOwnerCooldown && Context.User.Id == _config.OwnerID;

                if (!ignoreCooldown)
                {
                    var channelId = Context.Channel.Id;

                    if (_cooldowns.ContainsKey(channelId))
                    {
                        var elapsed = DateTime.UtcNow - _cooldowns[channelId];

                        if (cooldown.Value < elapsed)
                        {
                            await ReplyAsync(
                                "Slow down there dude! " +
                                $"You can only post a {category} every {Math.Round(cooldown.Value.TotalSeconds)} seconds!\n\n" +
                                $"If you want to send {category} with a shorter cooldown, please purchase donator status! Thanks!"
                            );
                            return;
                        }
                    }

                    _cooldowns[channelId] = DateTime.UtcNow;
                }
            }

            var embed = new EmbedBuilder()
                .WithTitle(image.Title)
                .WithImageUrl(image.Url)
                .Build();
            await ReplyAsync("", false, embed);
        }

        public async Task SetCommandEnabled(string command, bool enabled)
        {
            if (string.IsNullOrEmpty(command) || Context.User.Id != _config.OwnerID)
                return;

            var status = enabled ? "enable" : "disable";
            var commandLower = command.ToLower();

            if (enabled && _config.DisabledCommands.Contains(commandLower))
                _config.DisabledCommands.Remove(commandLower);
            else if (!enabled && !_config.DisabledCommands.Contains(commandLower))
                _config.DisabledCommands.Add(commandLower);
            else
            {
                await ReplyAsync($"{commandLower} isn't {status}d! How tf do you plan to {status} already {status}d commands??");
                return;
            }

            await ReplyAsync($"{commandLower} is now {status}d.");
        }

        #endregion
    }
}
