﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Oscar.Providers;

namespace Oscar.Modules
{
    public class AudioModule : ModuleBase
    {
        private static readonly Random _random = new Random();
        private static int _sfxCount = -1;
        private static List<string> _sfxLists = null;
        private readonly AudioProvider _audio;

        public AudioModule(AudioProvider audio) => _audio = audio;

        #region Commands

        [Command("cowboy")]
        [Summary("Yee-haw")]
        public async Task Cowboy() => await PlayCategory("cowboy").ConfigureAwait(false);

        [Command("dumbsong")]
        [Alias("shitsound")]
        public async Task DumbSong([Remainder]string song = null)
        {
            if (!string.IsNullOrEmpty(song))
                await Play("shitsound", song).ConfigureAwait(false);
            else
                await PlayRandom("shitsound").ConfigureAwait(false);
        }

        [Command("erb")]
        [Summary("Plays a random epic rap battles of history song.")]
        public async Task Erb() => await PlayRandom("erb").ConfigureAwait(false);

        [Command("fart")]
        [Summary("...")]
        public async Task Fart()
        {
            var success = await PlayRandom("farts").ConfigureAwait(false);

            if (success)
                await ReplyAsync("I recorded this one myself :wind_blowing_face:");
        }

        [Command("hey")]
        [Summary("Heyeyeyeyey - what's going on?")]
        public async Task Hey() => await PlayCategory("hey").ConfigureAwait(false);

        [Command("horn")]
        [Summary("Honk honk!")]
        public async Task Horn() => await PlayRandom("horns").ConfigureAwait(false);

        [Command("knock")]
        [Summary("Nick's favourite ;)")]
        public async Task Knock() => await PlayCategory("knock").ConfigureAwait(false);

        [Command("mlg")]
        [Summary("Plays a random MLG-montage sound effect in the current channel.")]
        public async Task Mlg() => await PlayRandom("mlg").ConfigureAwait(false);

        [Command("nice")]
        [Summary("Plays the \"Nice Meme\" sound effect in the current channel.")]
        public async Task NiceMeme() => await Play("meme", "nice").ConfigureAwait(false);

        [Command("oof")]
        [Summary("Plays the Roblox oof sound in the current channel.")]
        public async Task Oof()
        {
            if (await PlayCategory("oof").ConfigureAwait(false))
                await Context.Message.AddReactionAsync(new Emoji("💀"));
        }

        [Command("sfx")]
        [Summary("Plays a sound effect. To see a list of sound effects use the sfx list command.")]
        public async Task Sfx([Remainder] string effect = null)
        {
            if (effect == null)
                await ReplyAsync("u gotta provide an sfx file to play my man. use sfx list to see a list of available sound effects").ConfigureAwait(false);
            else if (effect.StartsWith("list", StringComparison.OrdinalIgnoreCase))
            {
                var pageNumberRaw = effect.After(' ');

                if (pageNumberRaw == null || !int.TryParse(pageNumberRaw, out var pageNumber))
                    pageNumber = 1;

                await SfxList(pageNumber).ConfigureAwait(false);
            }
            else
                if (await Play("sfx", effect).ConfigureAwait(false))
                    await Context.Message.AddReactionAsync(new Emoji("🔊"));

        }

        [Command("silence")]
        [Alias("quiet", "stop")]
        [Summary("Immediately stops playing the current sound effect")]
        public async Task Silence()
        {
            if (!(Context.User is IGuildUser user))
                return;

            if (_audio.IsPlayingAudio(user.VoiceChannel))
            {
                _audio.StopAudio(user.VoiceChannel);
                await ReplyAsync("okokok im leaving now, no need to be rude").ConfigureAwait(false);
            }
            else
                await ReplyAsync("I'm not playing anything right now though :(").ConfigureAwait(false);
        }

        [Command("unstuck")]
        [Summary("Disconnects the bot from all audio channels. Useful if Oscar gets stuck :(")]
        public async Task Unstuck() => await Task.Run(() => _audio.EmergencyDisconnect()).ConfigureAwait(false);

        [Command("wtf")]
        public async Task Wtf() => await PlayCategory("wtf").ConfigureAwait(false);

        #endregion

        #region Helper Methods

        private async Task<bool> Play(string category, string fileName)
        {
            if (!_audio.HasAudioFile(category, fileName))
            {
                await ReplyAsync("I don't have that sound effect >:(");
                return false;
            }

            // Join audio channel if the user is in one
            if (!(Context.User is IGuildUser guildUser))
                await ReplyAsync("I can't play audio files in DMs - use this command in a server we're in!");
            else if (guildUser.VoiceChannel == null)
                await ReplyAsync("You're not in a voice channel >:(");
            else if (_audio.IsPlayingAudio(guildUser.VoiceChannel))
                await ReplyAsync("Wait your turn >:(");
            else
            {
                // Discord.NET warns about not having command handlers take too long
                // to execute, so instead we start the task immediately and then bail out.
                //
                // TODO: Should something else be done about this?
                #pragma warning disable CS4014
                _audio.PlayAudioAsync(category, fileName, guildUser.VoiceChannel);
                #pragma warning restore CS4014
                return true;
            }

            return false;
        }

        private async Task<bool> PlayCategory(string category) => await Play(category, category);

        private async Task<bool> PlayRandom(string category)
        {
            var files = _audio.GetAudioFiles(category);

            if (files == null)
            {
                await ReplyAsync($"No {category} sounds available >:(");
                return false;
            }

            var sound = files[_random.Next(0, files.Length)];
            return await Play(category, Path.GetFileNameWithoutExtension(sound.Name));
        }

        private async Task SfxList(int pageNumber)
        {
            var sfxFiles = _audio.GetAudioFiles("sfx");

            if (sfxFiles == null)
            {
                await ReplyAsync("No audio files found :sob:");
                return;
            }

            if (_sfxLists == null || _sfxCount != sfxFiles.Length)
            {
                _sfxLists = new List<string>();
                _sfxCount = sfxFiles.Length;

                var builder = new StringBuilder();

                for (var x = 0; x < sfxFiles.Length; x++)
                {
                    var comma = x < sfxFiles.Length - 1 ? ", " : "";
                    var appendage = $"{Path.GetFileNameWithoutExtension(sfxFiles[x].Name)}{comma}";

                    if (builder.Length + appendage.Length > 2048)
                    {
                        _sfxLists.Add(builder.ToString());
                        builder = new StringBuilder();
                    }

                    builder.Append(appendage);
                }

                _sfxLists.Add(builder.ToString());
            }

            var adjustedPageNum = pageNumber - 1;

            if (adjustedPageNum < 0)
                await ReplyAsync("pages start at one, not zero my dude");
            else if (adjustedPageNum >= _sfxLists.Count)
                await ReplyAsync("sfx list doesn't have that many pages, dude");
            else
            {
                var description = _sfxLists[adjustedPageNum];
                var embed = new EmbedBuilder()
                    .WithDescription(description)
                    .WithTitle($"List of Sound Effects (page #{pageNumber})");
                await ReplyAsync("", false, embed.Build());
            }
        }

        #endregion
    }
}
