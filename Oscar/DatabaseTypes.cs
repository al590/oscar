﻿namespace Oscar
{
    public class Image
    {
        public string Url { get; }
        public string Title { get; }

        public Image(string url, string title)
        {
            Url = url;
            Title = title;
        }
    }
}
