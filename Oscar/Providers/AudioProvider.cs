﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Discord;
using Discord.Audio;

namespace Oscar.Providers
{
    public class AudioProvider
    {
        private const string FFMpegLaunchOptions = "-i \"{0}\" -hide_banner -loglevel error -nostats -ac 2 -f s16le -ar 48000 pipe:1";
        private readonly List<ulong> _activeAudioChannels = new List<ulong>();
        private readonly List<IAudioClient> _activeAudioClients = new List<IAudioClient>();
        private readonly object _activeAudioLock = new object();
        private readonly ConfigProvider _config;
        private readonly List<ulong> _stopQueue = new List<ulong>();

        public AudioProvider(ConfigProvider config) => _config = config;

        public Process OpenAudioFile(string path)
        {
            var ffmpegStartInfo = new ProcessStartInfo
            {
                FileName = "ffmpeg",
                Arguments = string.Format(FFMpegLaunchOptions, path),
                UseShellExecute = false,
                RedirectStandardOutput = true,
            };
            return Process.Start(ffmpegStartInfo) ?? 
                   throw new Exception("Failed to start ffmpeg!");
        }

        public void EmergencyDisconnect()
        {
            lock (_activeAudioLock)
            {
                _activeAudioChannels.Clear();
                _stopQueue.Clear();

                foreach (var client in _activeAudioClients)
                    client.Dispose();
            }
        }

        public FileInfo[] GetAudioFiles(string category)
        {
            var info = new DirectoryInfo(Path.Join(_config.AudioDirectory, category));
            return info.Exists ? info.GetFiles("*.opus") : null;
        }

        public string GetAudioPath(string soundType, string fileName) 
            => Path.Join(_config.AudioDirectory, soundType, fileName);

        public bool HasAudioFile(string category, string fileName) 
            => File.Exists(GetAudioPath(category, $"{fileName}.opus"));

        public bool IsPlayingAudio(IAudioChannel channel)
        {
            lock (_activeAudioLock)
                return _activeAudioChannels.Contains(channel.Id);
        }

        public async Task PlayAudioAsync(string category, string fileName, IAudioChannel channel)
        {
            var path = GetAudioPath(category, $"{fileName}.opus");
            IAudioClient audioClient = null;

            try
            {
                using (var ffmpeg = OpenAudioFile(path))
                {
                    // Discord.Audio recommends using PCM streams instead of sending
                    // already opus-encoded data for the time being until the opus
                    // methods have been ironed out.
                    using (audioClient = await channel.ConnectAsync())
                    using (var pcmStream = audioClient.CreatePCMStream(AudioApplication.Mixed))
                    {
                        lock (_activeAudioLock)
                        {
                            _activeAudioChannels.Add(channel.Id);
                            _activeAudioClients.Add(audioClient);
                        }

                        // Smaller buffer for a faster silence.
                        // Larger size will result in a longer delay before the
                        // bot disconnects after the silence command is issued
                        // from awaiting WriteAsync(). 
                        var buffer = new byte[512];
                        int readBytes;
                        var killed = false;

                        while ((readBytes = await ffmpeg.StandardOutput.BaseStream.ReadAsync(buffer, 0, buffer.Length)) > 0)
                        {
                            if (_stopQueue.Contains(channel.Id))
                            {
                                _stopQueue.Remove(channel.Id);
                                killed = true;
                                // Occasionally FFmpeg has a message regarding broken pipe,
                                // and while it doesn't crash the application this seems to
                                // stop the message from popping up.
                                ffmpeg.Kill();
                                break;
                            }

                            await pcmStream.WriteAsync(buffer, 0, readBytes);
                        }

                        // Immediately disconnect if killed - the user wants silence
                        if (!killed)
                            await pcmStream.FlushAsync();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error during audio playback: {e}");
            }
            finally
            {
                Task stopTask = null;

                lock (_activeAudioLock)
                {
                    _activeAudioChannels.Remove(channel.Id);

                    if (audioClient != null)
                    {
                        _activeAudioClients.Remove(audioClient);
                        stopTask = audioClient.StopAsync();
                    }
                }

                _stopQueue.Remove(channel.Id);

                if (stopTask != null)
                    await stopTask;
            }
        }

        public void StopAudio(IAudioChannel channel)
        {
            if (!_stopQueue.Contains(channel.Id))
                _stopQueue.Add(channel.Id);
        }
    }
}
