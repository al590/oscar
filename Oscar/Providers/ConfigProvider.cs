﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Oscar.Providers
{
    public class ConfigProvider
    {
        private readonly string _fileName;

        // Properties MUST have a setter if they are to be stored in the config
        // file. Removing the setter from the property will result in
        // Load() failing since JsonConvert.PopulateObject() will attempt to set
        // a value on a read-only property.
        public string AudioDirectory { get; set; } = "audio-files";
        public string CommandPrefix { get; set; } = "pls";
        public string ConnectionString { get; set; } = "Server=SERVER_IP_ADDRESS;User ID=USERNAME;Password=PASSWORD;Database=oscarbot";
        public List<string> DisabledCommands { get; set; } = new List<string>();
        public bool IgnoreOwnerCooldown { get; set; } = true;
        public ulong OwnerID { get; set; } = 0;
        public string Token { get; set; } = "";

        public ConfigProvider(string fileName) => _fileName = fileName;

        public bool IsValid()
        {
            if (string.IsNullOrEmpty(Token))
                Console.WriteLine("Oscar needs a bot token to be able to log in!");
            else if (string.IsNullOrEmpty(ConnectionString))
                Console.WriteLine("Oscar needs a valid database connection string.");
            else if (ConnectionString.Contains("SERVER_IP_ADDRESS", StringComparison.OrdinalIgnoreCase))
                Console.WriteLine("Please change the default connection string before using Oscar.");
            else if (OwnerID == 0)
                Console.WriteLine("Oscar needs to know his owner's ID before he can be used.");
            else
            {
                if (string.IsNullOrEmpty(CommandPrefix))
                {
                    Console.WriteLine("Command prefix was empty; using default value \"pls\"");
                    CommandPrefix = "pls";
                }
                else
                    CommandPrefix = CommandPrefix.Trim();

                return true;
            }

            return false;
        }

        public void Load()
        {
            JsonConvert.PopulateObject(File.ReadAllText(_fileName, Encoding.UTF8), this);
            DisabledCommands = DisabledCommands.Select(x => x.ToLower()).ToList();
        }

        public void Save() =>
            File.WriteAllText(_fileName, JsonConvert.SerializeObject(this, Formatting.Indented), Encoding.UTF8);
    }
}
