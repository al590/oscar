﻿using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Oscar.Providers
{
    public class DatabaseProvider
    {
        private const string QueryFindImages = 
            "SELECT url, title " +
            "FROM images " +
            "WHERE category = @category AND disabled = 0 " +
            "ORDER BY RAND() " +
            "LIMIT 1";

        private readonly ConfigProvider _config;

        public DatabaseProvider(ConfigProvider config) => _config = config;

        public async Task<MySqlConnection> ConnectAsync()
        {
            var connection = new MySqlConnection(_config.ConnectionString);
            await connection.OpenAsync();
            return connection;
        }

        public async Task<Image> FindRandomImageAsync(string category)
        {
            using (var connection = await ConnectAsync())
            using (var command = new MySqlCommand(QueryFindImages, connection))
            {
                command.Parameters.Add(new MySqlParameter("category", category));

                using (var reader = await command.ExecuteReaderAsync())
                {
                    return await reader.ReadAsync()
                        ? new Image(reader.GetString(0), reader.GetString(1))
                        : null;
                }
            }
        }
    }
}
